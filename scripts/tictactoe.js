var canvas, context;
var tiles;
var game;
var player;

/**
 * JQuery function to run code at the end of DOM loading.
 * Initializes global variables. Yes, I feel guilty.
 */
$(document).ready(function () {
    canvas = document.createElement("canvas");
    canvas.width = 380;
    canvas.height = 380;
    context = canvas.getContext("2d");
    document.getElementById('container').appendChild(canvas);

    game = new Game();
    init();
    tick();
    player = Tile.CIRCLE;
    game.assignTileValues();

    /**
     * Handles what to do when Tile is clicked.
     */
    $("canvas").bind("click", function (event) {
        var canvas = event.target;

        var mouseX = event.clientX - canvas.offsetLeft;
        var mouseY = event.clientY - canvas.offsetTop;

        if (mouseX % 120 >= 20 && mouseY % 120 >= 20) {
            var index = Math.floor(mouseX / 120);
            index += Math.floor(mouseY / 120) * 3;
            game.determineAndChangeTileState(index);
        }
    });
});

/**
 * Initializes tiles and pops them on the canvas
 */
function init() {
    if (tiles == null) {
        tiles = [];

        for (var i = 0; i < 9; i++) {
            var x = (i % 3) * 120 + 20;
            var y = Math.floor(i / 3) * 120 + 20;
            tiles.push(new Tile(x, y));
        }
    }
}

/**
 * Defines callback for window.requestAnimationFrame and
 * renders tiles each time the window is updated.
 */
function tick() {
    window.requestAnimationFrame(tick);

    render();
}

/**
 * Draws each tile onto the canvas with appropriate symbols.
 */
function render() {
    for (var i = 0; i < tiles.length; i++) {
        tiles[i].draw(context);
    }
}

/**
 * Keeps track of winning and Symbol placement
 */
function Game() {

    var circleScore = 0;
    var crossScore = 0;

    /**
     * Visits each tile and assigns it a predetermined value to determine score.
     */
    this.assignTileValues = function () {
        var tileValue = 1;

        for (var i = 0; i < tiles.length; i++) {
            tiles[i].scoreValue = tileValue;
            tileValue = tileValue * 2;
        }
    }

    /**
     * Changes the players tile type.
     */
    this.changePlayer = function () {
        if (player === Tile.CIRCLE) {
            player = Tile.CROSS
        } else {
            player = Tile.CIRCLE
        }
    }

    /**
     * Displays a winning message to the user by
     * appending an html element to the body.
     * @param tileType
     *       The tile type that won the game.
     */
    this.displayWinningMessage = function (tileType) {
        if (tileType === Tile.CROSS && tiles != null) {
            $("body").append("<div>Crosses win!</div>");
        } else if (tileType == Tile.CIRCLE && tiles != null) {
            $("body").append("<div>Circles win!</div>");
        }
    }

    /**
     * Compares the players score, determined from adding scores of each tile they've flipped,
     * to a list of predetermined winning score values. Uses bitwise & to determine if score
     * contains a winning combination.
     * @param score
     *        The players total score
     * @param tileType
     *        The type of tile a player is using.
     */
    this.compareScoresToWinConditions = function (score, tileType) {
        var threeInARowConditions = [7, 56, 73, 84, 146, 273, 292, 448];
        var playerScore = score;
        var someoneWon = false;
        for (var i = 0; i < threeInARowConditions.length; i++) {
            if ((threeInARowConditions[i] & playerScore) === threeInARowConditions[i]) {
                this.displayWinningMessage(tileType);
                $('canvas').unbind();
                someoneWon = true;
            }
        }
		return someoneWon;
    }

    /**
     * Adds value of tile to its respective players score.
     * @param index
     *        index of the tile whose value is needed.
     */
    this.checkWinConditions = function (index) {
        if (player === Tile.CIRCLE) {
            circleScore += tiles[index].scoreValue;
            this.compareScoresToWinConditions(circleScore, player);
        } else {
            crossScore += tiles[index].scoreValue;
            this.compareScoresToWinConditions(crossScore, player);
        }
    }

    /**
     * Checks if a tile has data and animates tile as well as checking if the tile
     * is a winning tile for the player. Changes the player after the move.
     * @param index
     *        The tile that needs to change state.
     * @returns the tile at the index
     */
    this.determineAndChangeTileState = function (index) {
        if (!(tiles[index].hasData())) {
            tiles[index].animate(player);
            this.checkWinConditions(index);
            this.changePlayer();
        }

        return tiles[index].getTile();

    }
}

/**
 * Represents a square in tic tac toe.
 * Knows what symbol is on the Tile and the Tile's score.
 * Keeps track of x and y location of the tile.
 * @param x
 *        The x location to place the Tile
 * @param y
 *        The y location to place the Tile
 */
function Tile(x, y) {
    var x = x;
    var y = y;
    var tile = Tile.BLANK;
    var animation = 0;
    var scoreValue = 0;

    /**
     * Colors the rectangle only. Does not draw symbol.
     */
    this.createBlankTile = function () {
        tileContext.fillRect(0, 0, 100, 100);
        Tile.BLANK = new Image();
        Tile.BLANK.src = tileCanvas.toDataURL();
    }

    /**
     * Draws a circle on the Tile
     */
    this.createCircleTile = function () {
        tileContext.fillRect(0, 0, 100, 100);
        tileContext.beginPath();
        tileContext.arc(50, 50, 30, 0, 2 * Math.PI);
        tileContext.stroke();
        Tile.CIRCLE = new Image();
        Tile.CIRCLE.src = tileCanvas.toDataURL();
    }

    /**
     * Draws a cross on the Tile
     */
    this.createCrossTile = function () {
        tileContext.fillRect(0, 0, 100, 100);
        tileContext.beginPath();
        tileContext.moveTo(20, 20);
        tileContext.lineTo(80, 80);
        tileContext.moveTo(80, 20);
        tileContext.lineTo(20, 80);
        tileContext.stroke();
        Tile.CROSS = new Image();
        Tile.CROSS.src = tileCanvas.toDataURL();
    }

    /**
     * Determines if the tile is blank or not.
     * @returns true if tile is not blank
     */
    this.hasData = function () {
        return tile !== Tile.BLANK;
    }

    if (tile == null) {
        var tileCanvas = document.createElement("canvas");
        var tileContext = tileCanvas.getContext("2d");

        tileCanvas.width = 100;
        tileCanvas.height = 100;

        tileContext.fillStyle = "#00FF99";
        tileContext.lineWidth = 5;
        tileContext.strokeStyle = "white";

        this.createBlankTile();
        this.createCircleTile();
        this.createCrossTile();

        tile = Tile.BLANK;
    }

    /**
     * Animates the shape by telling the Tile that it is now the specified type.
     * @param tileType
     *        The type of symbol to give this Tile
     */
    this.animate = function (tileType) {
        tile = tileType;
        animation = 1;
    }

    /**
     * Updates canvas after symbol is drawn
     */
    this.update = function () {
        if (animation > 0) {
            animation -= 1;
        }
    }

    /**
     * Draws the symbol onto the tile
     * @param context
     *        The context object of the canvas.
     */
    this.draw = function (context) {
        context.drawImage(tile, x, y);
    }

    /**
     * Gets itself and returns its type.
     * @returns the tiletype
     */
    this.getTile = function () {
        return tile;
    }

}