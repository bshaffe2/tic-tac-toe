describe("Test changing tile state on empty tiles.", function() {
    it("", function() {

        var index = 0;
        var game = new Game();
        init();

        game.determineAndChangeTileState(index);
        expect(tiles[index].hasData()).toEqual(true);

        index = 4;

        game.determineAndChangeTileState(index);
        expect(tiles[index].hasData()).toEqual(true);

        index = 8;

        game.determineAndChangeTileState(index);
        expect(tiles[index].hasData()).toEqual(true);

    });
});