describe("Test comparing scores to winning conditions.", function() {
  it("", function() {
    
	var winningScore = 473;
	var anotherWinningScore = 15;
	var losingScore = 169;
	var anotherLosingScore = 200;
	var game = new Game();

    var result = game.compareScoresToWinConditions(winningScore, Tile.CROSS);
    expect(result).toEqual(true);

    var result = game.compareScoresToWinConditions(anotherWinningScore, Tile.CROSS);
    expect(result).toEqual(true);

    var result = game.compareScoresToWinConditions(losingScore, Tile.CROSS);
    expect(result).toEqual(false);

    var result = game.compareScoresToWinConditions(anotherLosingScore, Tile.CROSS);
    expect(result).toEqual(false);
  });
});